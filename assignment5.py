#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify, Response
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        index = 0
        while index < len(books) and books[index]['id'] == str(index + 1):  
            index += 1  # loop until a missing id is found
        result = {'title': request.form['name'], 'id':str(index + 1)}
        books.insert(index, result)
        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        for b in books:  # find the book with the indicated id
            if b['id'] == str(book_id):
                b['title'] = request.form['name']
                break
        return redirect(url_for('showBook'))
    else:	
        for b in books:  # find the title of the book to edit
            if b['id'] == str(book_id):
                name = b['title']
                break
        return render_template('editBook.html', id = book_id, name = name)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        for b in books:
            if b['id'] == str(book_id):
                books.remove(b)
                break
        return redirect(url_for('showBook'))
    else:	
        for b in books:
            if b['id'] == str(book_id):
                name = b['title']
                break
        return render_template('deleteBook.html', id = book_id, name = name)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)